package com.p0mami.services.approximation;

import com.p0mami.models.processes.StochasticProcessPolyApproximation;
import com.p0mami.models.realizations.StochasticProcessGeneralRealization;
import com.p0mami.models.realizations.StochasticProcessStreamFunctionalRealization;
import com.p0mami.models.realizations.StochasticProcessStreamRealization;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.linear.SingularValueDecomposition;

import java.util.concurrent.atomic.AtomicReference;

public class PolyApproximationService {

    public StochasticProcessPolyApproximation approximate(StochasticProcessGeneralRealization realization, int k) {
        RealVector X = MatrixUtils.createRealVector(realization.getX());
        RealVector t = MatrixUtils.createRealVector(realization.getT());
        RealMatrix Z = buildZ(t, k);

        RealMatrix A = Z.multiply(Z.transpose());
        RealVector b = Z.transpose().preMultiply(X);

        SingularValueDecomposition svd = new SingularValueDecomposition(A);
        RealVector betta = svd.getSolver().solve(b);
        return new StochasticProcessPolyApproximation(betta.toArray(), 0);
    }

    private RealMatrix buildZ(RealVector t, int k) {
        int n = t.getDimension();
        double[][] zData = new double[k+1][n];

        for (int col = 0; col < n; col++) {
            double t_n = t.getEntry(col);
            double t_n_k = 1;
            for (int row = 0; row < k+1; row++) {
                zData[row][col] = t_n_k;
                t_n_k *= t_n;
            }
        }

        return MatrixUtils.createRealMatrix(zData);
    }

    public StochasticProcessPolyApproximation approximate(StochasticProcessStreamRealization realization, int k) {
        k += 1;
        double[][] aData = new double[k][k];
        double[] bDate = new double[k];
        int finalK = k;
        AtomicReference<Double> percent = new AtomicReference<>((double) 0);
//        realization.getTimeStream()
//                .peek(s -> {
//                    if (percent.get() + 0.1f < (s / realization.getSize() * 100)) {
//                        double percentValue = percent.get();
//                        System.out.print(String.format("%.1f", percentValue) + "%\r");
//                        percent.set(percentValue + 0.1);
//                    }
//                })
//                .peek((t) -> {
//            double tPow = 1;
//            for (int i = 0; i < finalK; i++) {
//                bDate[i] += tPow * realization.getGetX().applyAsDouble(t);
//                double tPow2 = 1;
//                for (int j = 0; j < finalK; j++) {
//                    aData[i][j] += tPow * tPow2;
//                    tPow2 *= t;
//                }
//                tPow *= t;
//            }
//        }).filter((t) -> false).findFirst();

        realization.getPointsStream()
                .peek(s -> {
                    if (percent.get() + 0.1f < (s.getFirst() / realization.getSize() * 100)) {
                        double percentValue = percent.get();
                        System.out.print(String.format("%.1f", percentValue) + "%\r");
                        percent.set(percentValue + 0.1);
                    }
                })
                .peek((p) -> {
                    double t = p.getFirst();
                    double X = p.getSecond();
                    double tPow = 1;
                    for (int i = 0; i < finalK; i++) {
                        bDate[i] += tPow * X;
                        double tPow2 = 1;
                        for (int j = 0; j < finalK; j++) {
                            aData[i][j] += tPow * tPow2;
                            tPow2 *= t;
                        }
                        tPow *= t;
                    }
                }).filter((t) -> false).findFirst();


        RealMatrix A = MatrixUtils.createRealMatrix(aData);
        RealVector b = MatrixUtils.createRealVector(bDate);
        SingularValueDecomposition svd = new SingularValueDecomposition(A);
        RealVector betta = svd.getSolver().solve(b);
        return new StochasticProcessPolyApproximation(betta.toArray(), 0);
    }


}
