package com.p0mami.services.readers;

import com.p0mami.models.realizations.StochasticProcessStreamFileRealization;
import org.apache.commons.math3.util.Pair;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.util.stream.Stream;

public class CSVStochasticProcessStreamRealizationReader {

    public static final String COMMA_DELIMITER = ",";

    public StochasticProcessStreamFileRealization readRealization(String filename) {

        Stream<Pair<Double, Double>> points = null;
        try {
            File file = new File(filename);
            points = Files.lines(file.toPath()).sequential().map(l -> {
                String[] buf = l.split(",");
                return new Pair<>(Double.parseDouble(buf[0]), Double.parseDouble(buf[1]));
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new StochasticProcessStreamFileRealization(points);
    }



}
