package com.p0mami.services.writers;

import com.p0mami.models.realizations.StochasticProcessStreamRealization;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class CSVStochasticProcessStreamRealizationWriter {

    public static final String COMMA_DELIMITER = ",";

    public CSVStochasticProcessStreamRealizationWriter() {
    }

    public void write(StochasticProcessStreamRealization realization, String filename) {

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true));) {
            realization.getPointsStream().forEachOrdered(
                    p -> {
                        try {
                            writer.write(p.getFirst() + ", "+ p.getSecond());
                            writer.newLine();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
            );
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
