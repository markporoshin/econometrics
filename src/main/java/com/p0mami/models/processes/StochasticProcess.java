package com.p0mami.models.processes;

import org.apache.commons.math3.util.Pair;

public abstract class StochasticProcess {

    public abstract Pair<Double, Double> getPoint(Double t);

    public abstract double getX(double t);

}
