package com.p0mami.models.processes;

import org.apache.commons.math3.util.Pair;


public class StochasticProcessPolyApproximation extends StochasticProcess {

    double[] betta;

    double sigma;

    @Override
    public double getX(double t) {
        double X = 0;
        double tPow = 1;
        for (int i = 0; i < betta.length; i++) {
            X += betta[i] * tPow;
            tPow *= t;
        }
        return X;
    }

    public StochasticProcessPolyApproximation(double[] betta, double sigma) {
        this.betta = betta;
        this.sigma = sigma;
    }

    @Override
    public Pair<Double, Double> getPoint(Double t) {
        return new Pair<>(t, getX(t));
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("{\n\tg(t) = ");
        for (int i = 0; i < betta.length; i++) {
            builder.append(String.format(" + %.2f", betta[i]));
            builder.append("*t^");
            builder.append(i);
        }
        builder.append("\n\t[");
        for (double v : betta) {
            builder.append(v);
            builder.append(", ");
        }
        builder.append("]\n\tsigma is ");
        builder.append(sigma);
        builder.append("\n}\n");

        return builder.toString();
    }
}
