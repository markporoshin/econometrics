package com.p0mami.models.processes;

import org.apache.commons.math3.util.Pair;

import java.util.function.DoubleUnaryOperator;

public class StochasticProcessGeneral extends StochasticProcess {

    private DoubleUnaryOperator g;

    private DoubleUnaryOperator e;

    public StochasticProcessGeneral() {
    }

    public StochasticProcessGeneral(DoubleUnaryOperator g, DoubleUnaryOperator e) {
        this.g = g;
        this.e = e;
    }

    @Override
    public double getX(double t) {
        return g.applyAsDouble(t) + e.applyAsDouble(t);
    }

    @Override
    public Pair<Double, Double> getPoint(Double t) {
        return new Pair<>(t, getX(t));
    }

}
