package com.p0mami.models.realizations;

import org.apache.commons.math3.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Supplier;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

public class StochasticProcessStreamFunctionalRealization extends StochasticProcessStreamRealization {

    private final DoubleUnaryOperator getX;
    private final Supplier<DoubleStream> dataGenerator;
    private final int size;

    public StochasticProcessStreamFunctionalRealization(Supplier<DoubleStream> dataGenerator, DoubleUnaryOperator getX) {
        this.getX = getX;
        this.dataGenerator = dataGenerator;
        this.size = 0;
    }

    public StochasticProcessStreamFunctionalRealization(Supplier<DoubleStream> dataGenerator, DoubleUnaryOperator getX, int size) {
        this.getX = getX;
        this.dataGenerator = dataGenerator;
        this.size = size;
    }

    public List<Pair<Double, Double>> getPoints() {
        double[] timePoints = dataGenerator.get().toArray();
        List<Pair<Double, Double>> result = new ArrayList<>();
        for (double time: timePoints) {
            result.add(new Pair<>(time, getX.applyAsDouble(time)));
        }
        return result;
    }

    public DoubleUnaryOperator getGetX() {
        return getX;
    }

    public DoubleStream getTimeStream() {
        return dataGenerator.get();
    }

    public Stream<Pair<Double, Double>> getPointsStream() {
        return dataGenerator.get().mapToObj(t -> new Pair<Double, Double>(t, getX.applyAsDouble(t)));
    }

    public int getSize() {
        return size;
    }
}
