package com.p0mami.models.realizations;

import org.apache.commons.math3.util.Pair;

import java.util.List;

public class StochasticProcessGeneralRealization {

    private List<Pair<Double, Double>> points;

    public void addPoint(Pair<Double, Double> point) {
        points.add(point);
    }

    public StochasticProcessGeneralRealization() {

    }

    public List<Pair<Double, Double>> getPoints() {
        return points;
    }

    public StochasticProcessGeneralRealization(List<Pair<Double, Double>> points) {
        this.points = points;
    }

    public double[] getT() {
        return points.stream().mapToDouble(Pair::getFirst).toArray();
    }

    public double[] getX() {
        return points.stream().mapToDouble(Pair::getSecond).toArray();
    }
}
