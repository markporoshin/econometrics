package com.p0mami.models.realizations;

import org.apache.commons.math3.util.Pair;

import java.util.stream.Stream;

public class StochasticProcessStreamFileRealization extends StochasticProcessStreamRealization {

    Stream<Pair<Double, Double>> pointStream;

    public StochasticProcessStreamFileRealization(Stream<Pair<Double, Double>> pointStream) {
        this.pointStream = pointStream;
    }

    @Override
    public Stream<Pair<Double, Double>> getPointsStream() {
        return pointStream;
    }

    @Override
    public int getSize() {
        return 1;
    }

    public void close() {
        pointStream.close();
    }
}
