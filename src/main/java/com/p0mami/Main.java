package com.p0mami;

import com.p0mami.models.processes.StochasticProcessGeneral;
import com.p0mami.models.processes.StochasticProcessPolyApproximation;
import com.p0mami.models.realizations.StochasticProcessStreamFunctionalRealization;
import com.p0mami.models.realizations.StochasticProcessStreamRealization;
import com.p0mami.services.approximation.PolyApproximationService;
import com.p0mami.services.providers.StochasticProcessRealizationProvider;
import com.p0mami.services.writers.CSVStochasticProcessStreamRealizationWriter;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.util.Random;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Supplier;
import java.util.stream.DoubleStream;

public class Main {

    private static final Random random = new Random();

    public static void main(String[] args) {
        double[] koefs = {10., 0.6, 0.9, 0.01};
        DoubleUnaryOperator g = (t) -> koefs[0] * t*t*t + koefs[1] * t*t + koefs[2] * t + koefs[3];

        double mean = 0;
        double sigma = 1;
        random.setSeed(32);
        DoubleUnaryOperator e = (t) -> random.nextGaussian() * sigma + mean;

        int size = 100;
        float step = 0.01f;



        Supplier<DoubleStream> d = () -> DoubleStream.iterate(0, c -> c + step).sequential().limit(size);
//        List<Double> d = DoubleStream.iterate(0, c -> c + step).limit(size).boxed().collect(Collectors.toList());

        StochasticProcessGeneral process = new StochasticProcessGeneral(g, e);
        StochasticProcessRealizationProvider provider = new StochasticProcessRealizationProvider(process);
        StochasticProcessStreamFunctionalRealization generalRealization = provider.provide(d, (int)(size * step));

        PolyApproximationService approximationService = new PolyApproximationService();
        StochasticProcessPolyApproximation approximation = approximationService.approximate(generalRealization, 3);

        StochasticProcessRealizationProvider approximationProvider = new StochasticProcessRealizationProvider(approximation);
        StochasticProcessStreamRealization approximationRealization = approximationProvider.provide(d, (int)(size * step));

        CSVStochasticProcessStreamRealizationWriter writer = new CSVStochasticProcessStreamRealizationWriter();
        writer.write(approximationRealization, "output.csv");

        System.out.println("Approximation:");
        System.out.println(approximation);


//        XYSeries generalSeries = new XYSeries("general");
//        generalRealization.getPoints().forEach(point -> generalSeries.add(point.getFirst(), point.getSecond()));
//        XYSeries approximationSeries = new XYSeries("approximation");
//        approximationRealization.getPointsStream().forEach(point -> approximationSeries.add(point.getFirst(), point.getSecond()));
//
//        XYSeriesCollection collection = new XYSeriesCollection(generalSeries);
//        collection.addSeries(approximationSeries);
//
//        JFreeChart generalChart = ChartFactory
//                .createXYLineChart("Stochastic Process Realization", "t", "X",
//                        collection,
//                        PlotOrientation.VERTICAL,
//                        true, true, true);
//
//        JFrame frame = new JFrame("Window");
//        frame.setLayout( new FlowLayout() );
//        frame.getContentPane().add(new ChartPanel(generalChart));
//        frame.pack();
//        frame.setSize(400,300);
//        frame.show();

    }

}
