package com.p0mami;

import com.p0mami.models.processes.StochasticProcessPolyApproximation;
import com.p0mami.models.realizations.StochasticProcessStreamFileRealization;
import com.p0mami.services.approximation.PolyApproximationService;
import com.p0mami.services.readers.CSVStochasticProcessStreamRealizationReader;

public class FromFileMain {

    public static void main(String[] args) {
        CSVStochasticProcessStreamRealizationReader reader = new CSVStochasticProcessStreamRealizationReader();
        StochasticProcessStreamFileRealization fileRealization = reader.readRealization("input.csv");
        PolyApproximationService approximationService = new PolyApproximationService();
        StochasticProcessPolyApproximation approximation = approximationService.approximate(fileRealization, 3);
        System.out.println(approximation);
        fileRealization.close();
    }

}
